USE mydatabase;

-- Utwórz tabelę
CREATE TABLE IF NOT EXISTS mytable (
	    id INT AUTO_INCREMENT PRIMARY KEY,
	    name VARCHAR(255) NOT NULL,
	    price INT NOT NULL
	);

	-- Wstaw przykładowe dane
INSERT INTO mytable (name, price) VALUES ('Dixit', 125);
INSERT INTO mytable (name, price) VALUES ('Bang', 80);
INSERT INTO mytable (name, price) VALUES ('Bang 2', 280);
INSERT INTO mytable (name, price) VALUES ('Splendor', 380);
