from flask import Flask, render_template, jsonify, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf import FlaskForm, CSRFProtect  # Dodaj import CSRFProtect
from wtforms import StringField, IntegerField, SubmitField
from flask import redirect


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://myuser:mypassword@mysql/mydatabase'
app.config['SECRET_KEY'] = 'jadamisiejada'  # Dodaj sekretny klucz CSRF
db = SQLAlchemy(app)
migrate = Migrate(app, db)
csrf = CSRFProtect(app)  # Dodaj CSRFProtect

class MyTable(db.Model):
    __tablename__ = 'mytable'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    price = db.Column(db.Integer)

class AddItemForm(FlaskForm):
    name = StringField('Nazwa')
    price = IntegerField('Cena')
    submit = SubmitField('Dodaj')

@app.route('/')
def hello_world():
    return jsonify(message='Hello, World!')

@app.route('/get_data_from_db')
def get_data_from_db():
    data_from_db = MyTable.query.all()
    form = AddItemForm()
    return render_template('table.html', data=data_from_db, form=form)

@app.route('/add_item', methods=['POST'])
def add_item():
    form = AddItemForm(request.form)

    if form.validate():
        new_item = MyTable(
            name=form.name.data,
            price=form.price.data
        )
        db.session.add(new_item)
        db.session.commit()

    return redirect(url_for('get_data_from_db'))

@app.route('/delete_item/<int:id>', methods=['GET'])
def delete_item(id):
    item = MyTable.query.get(id)
    if item:
        db.session.delete(item)
        db.session.commit()
    return redirect('/get_data_from_db')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

