Project DevOps Demonstration Environment

3 tier application

Frontend - ?
Backend - ?
Database - ?

Idea:
Frontend - strona wizualna dla projektu
Backend - obsługuje zapytania z przycisków na froncie
Database - data output/input

Frontend:
    Zawartość bazy danych wyświetla się od razu, automatycznie. 
    Do dodawania bazy danych, prosty formularz typu: id, nazwa, cena i przycisk do dodania elementu do bazy danych.
Backend: 
    Dwie funkcje, wyświetlanie wyników z bazy danych oraz dodawanie wyników z bazy danych.
Database:
    Jedna tabela z tzema kolumnami. 

Stack:
    Frontend/Database/Backend - kwestia dowolna
    Skonteneryzowana aplikacja uruchamiana lokalnie w przeglądarce - Docker/docker-compose.yml
    
Tips:
    Dockerfile dla każdego mikroserwisu, docker-compose.yml dla całości
    Przykładowy docker-compose
    
